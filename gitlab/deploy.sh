mvn clean compile deploy
mvn compile -pl marketplace-comment-service -U com.google.cloud.tools:jib-maven-plugin:3.2.1:build -Djib.to.auth.username=$DOCKER_USERNAME -Djib.to.auth.password=$DOCKER_PASS  -Dimage=batmaaan/marketplace-comment-service:1.0-SNAPSHOT
cd /home/finch/app/comment; docker-compose pull; docker-compose stop; docker-compose up -d;
