package org.marketplace.comment.client;

import org.marketplace.comment.client.operation.comment.CommentOperation;

public interface CommentServiceClient {

  CommentOperation comment();
}
