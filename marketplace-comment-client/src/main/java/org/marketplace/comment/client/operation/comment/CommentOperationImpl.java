package org.marketplace.comment.client.operation.comment;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.marketplace.comment.model.request.CommentRequest;
import org.marketplace.comment.model.response.CommentListResponse;
import org.marketplace.comment.model.response.CommentResponse;
import org.marketplace.comment.model.response.DeleteResponse;
import org.marketplace.comment.model.utils.Order;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

import static org.marketplace.comment.model.utils.UrlEndpoints.*;

public class CommentOperationImpl implements CommentOperation {
  ObjectMapper objectMapper = new ObjectMapper();
  private final HttpClient httpClient;
  private final String URL;


  public CommentOperationImpl(HttpClient httpClient, String url) {
    this.httpClient = httpClient;
    this.URL = url;
  }

  @Override
  @SneakyThrows
  public CommentResponse getCommentById(Integer id) {
    HttpRequest request = HttpRequest.newBuilder()
      .uri(new URI(URL + GET_COMMENT_BY_ID_URI + "?id=" + id))
      .GET()
      .build();

    var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
    return objectMapper.readValue(response.body(), CommentResponse.class);
  }


  @Override
  @SneakyThrows
  public CommentListResponse getCommentsByUser(Integer userId, int limit, int offset, Order order) {
    HttpRequest request = HttpRequest.newBuilder()
      .uri(new URI(URL + GET_COMMENT_BY_USER_URI + "?userId=" + userId + "&limit=" + limit + "&offset=" + offset + "&order=" + order.toString()))
      .GET()
      .build();

    var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
    return objectMapper.readValue(response.body(), CommentListResponse.class);
  }

  @Override
  @SneakyThrows
  public CommentListResponse getCommentsByProduct(Integer productId, int limit, int offset, Order order) {
    HttpRequest request = HttpRequest.newBuilder()
      .uri(new URI(URL + GET_COMMENT_BY_PRODUCT_URI + "?productId=" + productId + "&limit=" + limit + "&offset=" + offset + "&order=" + order.toString()))
      .GET()
      .build();

    var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
    return objectMapper.readValue(response.body(), CommentListResponse.class);
  }

  @Override
  @SneakyThrows
  public CommentResponse saveComment(CommentRequest commentRequest) {
    String json = objectMapper.writeValueAsString(commentRequest);
    HttpRequest request = HttpRequest.newBuilder()
      .uri(new URI(URL + SAVE_COMMENT_URI))
      .timeout(Duration.ofSeconds(5))
      .header("Content-Type", "application/json")
      .POST(HttpRequest.BodyPublishers.ofString(json))
      .build();

    var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
    return objectMapper.readValue(response.body(), CommentResponse.class);
  }

  @Override
  @SneakyThrows
  public DeleteResponse deleteById(Integer id, Integer productId, Integer userId) {
    HttpRequest request = HttpRequest.newBuilder()
      .uri(new URI(URL + DELETE_COMMENT_URI + "?id=" + id + "&productId=" + productId + "&userId=" + userId))
      .DELETE()
      .build();

    var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
    return objectMapper.readValue(response.body(), DeleteResponse.class);
  }
}
