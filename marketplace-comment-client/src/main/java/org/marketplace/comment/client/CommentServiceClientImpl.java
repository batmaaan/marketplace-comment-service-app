package org.marketplace.comment.client;

import org.marketplace.comment.client.operation.comment.CommentOperation;
import org.marketplace.comment.client.operation.comment.CommentOperationImpl;

import java.net.http.HttpClient;
import java.time.Duration;

public class CommentServiceClientImpl implements CommentServiceClient {

  private final CommentOperation commentOperation;

  public CommentServiceClientImpl(String url) {
    HttpClient client = HttpClient.newBuilder()
      .version(HttpClient.Version.HTTP_1_1)
      .followRedirects(HttpClient.Redirect.NORMAL)
      .connectTimeout(Duration.ofSeconds(5))
      .build();

    this.commentOperation = new CommentOperationImpl(client, url);
  }

  @Override
  public CommentOperation comment() {
    return commentOperation;
  }
}
