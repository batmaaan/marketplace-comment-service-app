package org.marketplace.comment.client.operation.comment;

import org.marketplace.comment.model.response.CommentListResponse;
import org.marketplace.comment.model.response.CommentResponse;
import org.marketplace.comment.model.response.DeleteResponse;
import org.marketplace.comment.model.request.CommentRequest;
import org.marketplace.comment.model.utils.Order;

public interface CommentOperation {

  CommentResponse getCommentById(Integer id);


  CommentListResponse getCommentsByUser(Integer userId, int limit, int offset, Order order);

  CommentListResponse getCommentsByProduct(Integer productId, int limit, int offset, Order order);

  CommentResponse saveComment(CommentRequest commentRequest);

  DeleteResponse deleteById(Integer id,  Integer productId,  Integer userId);
}
