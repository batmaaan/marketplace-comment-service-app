package org.marketplace.comment.model.utils;

public enum Order {
  ASC, DESC
}
