package org.marketplace.comment.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.marketplace.comment.model.Comment;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentResponse {
  private String status;
  private String description;
  private Comment comment;
}
