package org.marketplace.comment.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentRequest {
  private Integer productId;
  private Integer userId;
  private String text;
}
