package org.marketplace.comment.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.marketplace.comment.model.Comment;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentListResponse {
  private String status;
  private String description;
  private boolean hasNext;
  private List<Comment> comments;
}
