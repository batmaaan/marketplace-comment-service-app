package org.marketplace.comment.model;

import lombok.*;

import java.util.Date;

@Data
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Comment {
  private Integer id;
  private Integer productId;
  private Integer userId;
  private String text;
  private Date date;
}
