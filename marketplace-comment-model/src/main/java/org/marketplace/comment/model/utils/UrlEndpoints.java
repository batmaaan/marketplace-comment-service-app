package org.marketplace.comment.model.utils;

public class UrlEndpoints {
  private static final String COMMENTS_URI = "/comments";
  public static final String GET_COMMENT_BY_ID_URI = COMMENTS_URI+ "/by-id";
  public static final String GET_COMMENT_BY_USER_URI = COMMENTS_URI+ "/by-user";
  public static final String GET_COMMENT_BY_PRODUCT_URI = COMMENTS_URI+ "/by-product";
  public static final String GET_ALL_COMMENT_URI = COMMENTS_URI+ "/all";
  public static final String SAVE_COMMENT_URI = COMMENTS_URI+ "/save";
  public static final String DELETE_COMMENT_URI = COMMENTS_URI+ "/delete";
}
