package org.marketplace.comment.service.data.service;

import lombok.AllArgsConstructor;
import org.marketplace.comment.model.request.CommentRequest;
import org.marketplace.comment.model.response.CommentListResponse;
import org.marketplace.comment.model.response.CommentResponse;
import org.marketplace.comment.model.response.DeleteResponse;
import org.marketplace.comment.model.utils.Order;
import org.marketplace.comment.service.data.repository.CommentRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@AllArgsConstructor
public class CommentService {
  public static final String ERROR = "error";
  private static final String INCORRECT_PRODUCT_ID = "incorrect product ID";
  private static final String INCORRECT_USER_ID = "incorrect user ID";
  private static final String INCORRECT_ID = "incorrect id";

  private static final Mono<CommentResponse> INCORRECT_ID_RESPONSE = Mono.just(new CommentResponse(
    ERROR,
    INCORRECT_ID,
    null
  ));
  private static final Mono<CommentListResponse> INCORRECT_USER_ID_RESPONSE = Mono.just(new CommentListResponse(
    ERROR,
    INCORRECT_USER_ID,
    false,
    null
  ));
  private static final Mono<CommentListResponse> INCORRECT_PRODUCT_ID_RESPONSE = Mono.just(new CommentListResponse(
    ERROR,
    INCORRECT_PRODUCT_ID,
    false,
    null
  ));

  private final CommentRepository repository;

  public Mono<CommentResponse> getCommentById(Integer id) {
    if (id <= 0) {
      return INCORRECT_ID_RESPONSE;
    } else {
      return repository.getCommentById(id).map(comment -> new CommentResponse("ok", "", comment));
    }
  }


  public Mono<CommentListResponse> getCommentsByUser(Integer userId, int limit, int offset, Order order) {
    if (userId <= 0) {
      return INCORRECT_USER_ID_RESPONSE;
    } else {
      return repository.getCommentsByUser(userId, limit + 1, offset, order)
        .collectList()
        .map(list -> {
          if (list.size() > limit) {
            return new CommentListResponse(
              "ok",
              "",
              true,
              list.stream().limit(limit).toList()
            );
          } else {
            return new CommentListResponse(
              "ok",
              "",
              false,
              list.stream().limit(limit).toList()
            );
          }
        });
    }
  }

  public Mono<CommentListResponse> getCommentsByProduct(Integer productId, int limit, int offset, Order order) {
    if (productId == 0 || productId <= 0) {
      return INCORRECT_PRODUCT_ID_RESPONSE;
    } else {
      return repository.getCommentsByProduct(productId, limit + 1, offset, order)
        .collectList()
        .map(list -> {
          if (list.size() > limit) {
            return new CommentListResponse(
              "ok",
              "",
              true,
              list.stream().limit(limit).toList()
            );
          } else {
            return new CommentListResponse(
              "ok",
              "",
              false,
              list.stream().limit(limit).toList()
            );
          }
        });
    }
  }

  public Mono<CommentResponse> saveComment(CommentRequest commentRequest) {

    if (commentRequest.getUserId() == null) {
      return Mono.just(new CommentResponse(
        ERROR,
        INCORRECT_USER_ID,
        null
      ));
    }
    if (commentRequest.getProductId() == null) {
      return Mono.just(new CommentResponse(
        ERROR,
        INCORRECT_PRODUCT_ID,
        null
      ));
    }
    if (commentRequest.getText() == null || commentRequest.getText().length() < 5 || commentRequest.getText().equals("")) {
      return Mono.just(new CommentResponse(
        ERROR,
        "text must be not empty",
        null
      ));
    } else {
      return repository.saveComment(commentRequest).map(comment -> new CommentResponse(
        "ok",
        "comment saved",
        comment
      ));
    }
  }

  public Mono<DeleteResponse> deleteById(Integer id, Integer productId, Integer userId) {
    if (id <= 0) {
      return Mono.just(new DeleteResponse(ERROR, "comment not found"));
    } else {
      return repository.deleteById(id, productId, userId).flatMap(rowsUpdated -> {
        if (rowsUpdated != 1) {
          return Mono.just(new DeleteResponse(ERROR, "comment not found"));
        } else return Mono.just(new DeleteResponse("ok", "comment deleted"));
      });
    }
  }
}


