package org.marketplace.comment.service.api;

import lombok.AllArgsConstructor;
import org.marketplace.comment.service.data.service.CommentService;
import org.marketplace.comment.model.request.CommentRequest;
import org.marketplace.comment.model.response.CommentListResponse;
import org.marketplace.comment.model.response.CommentResponse;
import org.marketplace.comment.model.response.DeleteResponse;
import org.marketplace.comment.model.utils.Order;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import static org.marketplace.comment.model.utils.UrlEndpoints.*;

@RestController
@AllArgsConstructor
public class CommentController {

  private final CommentService service;

  @GetMapping(GET_COMMENT_BY_ID_URI)
  public Mono<CommentResponse> getCommentById(@RequestParam Integer id) {
    return service.getCommentById(id);
  }


  @GetMapping(GET_COMMENT_BY_USER_URI)
  public Mono<CommentListResponse> getCommentsByUser(
    @RequestParam Integer userId,
    @RequestParam int limit,
    @RequestParam int offset,
    @RequestParam(defaultValue = "desc") Order order) {
    return service.getCommentsByUser(userId, limit, offset, order);
  }

  @GetMapping(GET_COMMENT_BY_PRODUCT_URI)
  public Mono<CommentListResponse> getCommentsByProduct(
    @RequestParam Integer productId,
    @RequestParam int limit,
    @RequestParam int offset,
    @RequestParam(defaultValue = "desc") Order order) {
    return service.getCommentsByProduct(productId, limit, offset, order);
  }

  @PostMapping(SAVE_COMMENT_URI)
  public Mono<CommentResponse> saveComment(@RequestBody CommentRequest commentRequest) {
    return service.saveComment(commentRequest);
  }

  @DeleteMapping(DELETE_COMMENT_URI)
  public Mono<DeleteResponse> deleteById(@RequestParam Integer id, @RequestParam Integer productId, @RequestParam Integer userId) {
    return service.deleteById(id, productId, userId);
  }

}
