package org.marketplace.comment.service.data.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.r2dbc.postgresql.api.PostgresqlConnection;
import io.r2dbc.postgresql.api.PostgresqlResult;
import lombok.AllArgsConstructor;
import org.marketplace.comment.model.Comment;
import org.marketplace.comment.model.request.CommentRequest;
import org.marketplace.comment.model.utils.Order;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Date;

@Repository
@AllArgsConstructor
public class CommentRepository {
  private final Mono<PostgresqlConnection> postgresqlConnection;
  ObjectMapper objectMapper = new ObjectMapper();
  private static final String ID = "id";
  private static final String PRODUCT_ID = "product_id";
  private static final String USER_ID = "user_id";
  private static final String TEXT = "text";
  private static final String DATE = "date";

  private static final int TABLE_COUNT = 10;


  public Mono<Comment> getCommentById(Integer id) {
    return postgresqlConnection.flatMapMany(connection -> connection.createStatement(
          "select * from comments_" + 0 + " where id=$1"
            + " union select * from comments_" + 1 + " where id=$1"
            + " union select * from comments_" + 2 + " where id=$1"
            + " union select * from comments_" + 3 + " where id=$1"
            + " union select * from comments_" + 4 + " where id=$1"
            + " union select * from comments_" + 5 + " where id=$1"
            + " union select * from comments_" + 6 + " where id=$1"
            + " union select * from comments_" + 7 + " where id=$1"
            + " union select * from comments_" + 8 + " where id=$1"
            + " union select * from comments_" + 9 + " where id=$1"
        )
        .bind("$1", id)
        .execute())
      .flatMap(postgresqlResult -> postgresqlResult.map(s -> Comment.builder()
        .id((Integer) s.get(ID))
        .userId((Integer) s.get(USER_ID))
        .productId((Integer) s.get(PRODUCT_ID))
        .text((String) s.get(TEXT))
        .date(objectMapper.convertValue(s.get(DATE), Date.class))
        .build()))
      .next();
  }

  public Flux<Comment> getCommentsByUser(Integer userId, int limit, int offset, Order order) {

    return postgresqlConnection.flatMapMany(connection -> connection.createStatement(
        "SELECT * FROM comments_" + 0 +
          " union SELECT * FROM comments_" + 1 +
          " union SELECT * FROM comments_" + 2 +
          " union SELECT * FROM comments_" + 3 +
          " union SELECT * FROM comments_" + 4 +
          " union SELECT * FROM comments_" + 5 +
          " union SELECT * FROM comments_" + 6 +
          " union SELECT * FROM comments_" + 7 +
          " union SELECT * FROM comments_" + 8 +
          " union SELECT * FROM comments_" + 9 +
          " where user_id=$1 order by date " + order.toString() + " limit $2 offset $3"
      )
      .bind("$1", userId)
      .bind("$2", limit)
      .bind("$3", offset)
      .execute()).flatMap(postgresqlResult -> postgresqlResult.map(s -> Comment.builder()
      .id((Integer) s.get(ID))
      .userId((Integer) s.get(USER_ID))
      .productId((Integer) s.get(PRODUCT_ID))
      .text((String) s.get(TEXT))
      .date(objectMapper.convertValue(s.get(DATE), Date.class))
      .build()));
  }

  public Flux<Comment> getCommentsByProduct(Integer productId, int limit, int offset, Order order) {

    int shard = productId % TABLE_COUNT;
    return postgresqlConnection.flatMapMany(connection -> connection.createStatement(
        "SELECT * FROM comments_" + shard + " where product_id=$1 order by date "
          + order.toString() + " limit $2 offset $3")
      .bind("$1", productId)
      .bind("$2", limit)
      .bind("$3", offset)
      .execute()).flatMap(postgresqlResult -> postgresqlResult.map(s -> Comment.builder()
      .id((Integer) s.get(ID))
      .userId((Integer) s.get(USER_ID))
      .productId((Integer) s.get(PRODUCT_ID))
      .text((String) s.get(TEXT))
      .date(objectMapper.convertValue(s.get(DATE), Date.class))
      .build()));
  }

  public Mono<Comment> saveComment(CommentRequest commentRequest) {
    final Integer productId = commentRequest.getProductId();

    int shard = productId % TABLE_COUNT;
    return postgresqlConnection.flatMapMany(connection -> {
      Date newDate = new Date();
      return connection.createStatement("INSERT INTO  comments_" + shard + " (product_id, user_id, text, date) VALUES " +
          "($1, $2, $3, $4) returning id, product_id, user_id, text, date")
        .bind("$1", commentRequest.getProductId())
        .bind("$2", commentRequest.getUserId())
        .bind("$3", commentRequest.getText())
        .bind("$4", newDate)
        .execute();
    }).flatMap(postgresqlResult -> postgresqlResult.map(s -> Comment.builder()
      .id((Integer) s.get(ID))
      .productId((Integer) s.get(PRODUCT_ID))
      .userId((Integer) s.get(USER_ID))
      .text((String) s.get(TEXT))
      .date(new Date())
      .build())).next();
  }

  public Mono<Integer> deleteById(Integer id, Integer productId, Integer userId) {
    int shard = productId % TABLE_COUNT;
    return postgresqlConnection.flatMapMany(connection -> connection.createStatement(
        "DELETE FROM comments_" + shard + " WHERE id=$1 and user_id=$2"
      )
      .bind("$1", id)
      .bind("$2", userId)
      .execute()).flatMap(PostgresqlResult::getRowsUpdated).next();
  }
}


