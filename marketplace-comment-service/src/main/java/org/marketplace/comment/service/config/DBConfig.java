package org.marketplace.comment.service.config;


import io.r2dbc.postgresql.PostgresqlConnectionConfiguration;
import io.r2dbc.postgresql.PostgresqlConnectionFactory;
import io.r2dbc.postgresql.api.PostgresqlConnection;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Mono;

@Configuration
public class DBConfig {


  @Bean
  public PostgresqlConnectionFactory connectionFactory(
    @Value("${app.datasource.username}") String username,
    @Value("${app.datasource.password}") String password,
    @Value("${app.datasource.host}") String host,
    @Value("${app.datasource.database}") String database,
    @Value("${app.datasource.port:5432}") int port
  ) {
    // postgres
    return new PostgresqlConnectionFactory(PostgresqlConnectionConfiguration.builder()
      .host(host)
      .port(port)
      .database(database)
      .username(username)
      .password(password)
      .build());
  }


  @Bean
  Mono<PostgresqlConnection> mono(PostgresqlConnectionFactory connection) {
  return connection.create();
  }



}
