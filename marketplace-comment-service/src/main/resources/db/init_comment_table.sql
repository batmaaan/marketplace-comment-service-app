create sequence comments_1_id_seq;
create table comments_1

(
  id         integer default nextval('comments_1_id_seq'::regclass) not null
    primary key,
  product_Id integer                                                not null,
  user_Id    integer                                                not null,
  text       varchar(200)                                           not null,
  date       timestamp                                              not null
);
create sequence comments_2_id_seq;

create table comments_2

(
  id         integer default nextval('comments_2_id_seq'::regclass) not null
    primary key,
  product_Id integer                                                not null,
  user_Id    integer                                                not null,
  text       varchar(200)                                           not null,
  date       timestamp                                              not null
);

create sequence comments_3_id_seq;

create table comments_3

(
  id         integer default nextval('comments_3_id_seq'::regclass) not null
    primary key,
  product_Id integer                                                not null,
  user_Id    integer                                                not null,
  text       varchar(200)                                           not null,
  date       timestamp                                              not null
);

create sequence comments_4_id_seq;

create table comments_4

(
  id         integer default nextval('comments_4_id_seq'::regclass) not null
    primary key,
  product_Id integer                                                not null,
  user_Id    integer                                                not null,
  text       varchar(200)                                           not null,
  date       timestamp                                              not null
);

create sequence comments_5_id_seq;

create table comments_5

(
  id         integer default nextval('comments_5_id_seq'::regclass) not null
    primary key,
  product_Id integer                                                not null,
  user_Id    integer                                                not null,
  text       varchar(200)                                           not null,
  date       timestamp                                              not null
);

create sequence comments_6_id_seq;

create table comments_6

(
  id         integer default nextval('comments_6_id_seq'::regclass) not null
    primary key,
  product_Id integer                                                not null,
  user_Id    integer                                                not null,
  text       varchar(200)                                           not null,
  date       timestamp                                              not null
);
create sequence comments_7_id_seq;

create table comments_7

(
  id         integer default nextval('comments_7_id_seq'::regclass) not null
    primary key,
  product_Id integer                                                not null,
  user_Id    integer                                                not null,
  text       varchar(200)                                           not null,
  date       timestamp                                              not null
);

create sequence comments_8_id_seq;

create table comments_8

(
  id         integer default nextval('comments_8_id_seq'::regclass) not null
    primary key,
  product_Id integer                                                not null,
  user_Id    integer                                                not null,
  text       varchar(200)                                           not null,
  date       timestamp                                              not null
);
create sequence comments_9_id_seq;

create table comments_9

(
  id         integer default nextval('comments_9_id_seq'::regclass) not null
    primary key,
  product_Id integer                                                not null,
  user_Id    integer                                                not null,
  text       varchar(200)                                           not null,
  date       timestamp                                              not null
);

create sequence comments_10_id_seq;

create table comments_10

(
  id         integer default nextval('comments_10_id_seq'::regclass) not null
    primary key,
  product_Id integer                                                 not null,
  user_Id    integer                                                 not null,
  text       varchar(200)                                            not null,
  date       timestamp                                               not null
);
