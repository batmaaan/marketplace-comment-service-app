package org.marketplace.comment.service.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.marketplace.comment.client.CommentServiceClient;
import org.marketplace.comment.client.CommentServiceClientImpl;
import org.marketplace.comment.model.request.CommentRequest;
import org.marketplace.comment.model.response.CommentResponse;
import org.marketplace.comment.model.utils.Order;
import org.marketplace.comment.service.AbstractIntegrationTest;
import org.marketplace.comment.service.data.service.CommentService;
import org.marketplace.utils.InitComment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class CommentControllerTest extends AbstractIntegrationTest {
  @LocalServerPort
  int randomServerPort;

  private CommentServiceClient client;

  @Autowired
  private CommentService service;

  @BeforeEach
  public void init() {
    if (client == null) {
      this.client = new CommentServiceClientImpl("http://localhost:" + randomServerPort);
    }
  }
//@AfterEach
//public void clearTables(){
//postgresqlConnection.flatMapMany(connection -> {
//  return connection.createStatement("DELETE from comments_0;" +
//    "DELETE from comments_1;" +
//    "DELETE from comments_2;" +
//    "DELETE from comments_3;" +
//    "DELETE from comments_4;" +
//    "DELETE from comments_5;" +
//    "DELETE from comments_6;" +
//    "DELETE from comments_7;" +
//    "DELETE from comments_8;" +
//    "DELETE from comments_9;")
//    .execute();
//});
//}
//  @Test
//  void getCommentById() {
//
//    var randomComment = InitComment.initRandomComment();
//    var comment = service.saveComment(
//      new CommentRequest(
//        randomComment.getProductId(),
//        randomComment.getUserId(),
//        randomComment.getText()
//      ));
//    var result = client.comment().getCommentById(comment.block().getComment().getId());
//
//    assertEquals("ok", result.getStatus());
//    assertEquals(comment.block().getComment().getProductId(), result.getComment().getProductId());
//    assertEquals(comment.block().getComment().getUserId(), result.getComment().getUserId());
//    assertEquals(comment.block().getComment().getText(), result.getComment().getText());
//    assertNotNull(result.getComment().getDate());
//
//  }
//
//  @Test
//  void getCommentByZeroId() {
//    var result = client.comment().getCommentById(0);
//
//    assertEquals("error", result.getStatus());
//    assertEquals("incorrect id", result.getDescription());
//    assertNull(result.getComment());
//  }
//
//  @Test
//  void getCommentByMinusId() {
//    var result = client.comment().getCommentById(-12);
//
//    assertEquals("error", result.getStatus());
//    assertEquals("incorrect id", result.getDescription());
//    assertNull(result.getComment());
//  }



  @Test
  void getCommentsByUserHasNextTrue() {
    var randomComment = InitComment.initRandomComment();
    for (int i = 0; i < 10; i++) {
      service.saveComment(
        new CommentRequest(
          randomComment.getProductId(),
          randomComment.getUserId(),
          randomComment.getText()
        )).share().block();
    }
    Random random = new Random();
    var limit = random.nextInt(4) + 1;
    var offset = random.nextInt(4) + 1;

    var result = client.comment().getCommentsByUser(randomComment.getUserId(), limit, offset, Order.DESC);

    assertEquals("ok", result.getStatus());
    assertTrue(result.isHasNext());
    assertNotNull(result.getComments());
  }

  @Test
  void getCommentsByUserHasNextFalse() {
    var randomComment = InitComment.initRandomComment();
    for (int i = 0; i < 10; i++) {
      service.saveComment(
        new CommentRequest(
          randomComment.getProductId(),
          randomComment.getUserId(),
          randomComment.getText()
        )).share().block();
    }
    var result = client.comment().getCommentsByUser(randomComment.getUserId(), 1000, 0, Order.DESC);

    assertEquals("ok", result.getStatus());
    assertFalse(result.isHasNext());
    assertNotNull(result.getComments());
  }

  @Test
  void getCommentsWithWrongUserId() {
    Random random = new Random();
    var limit = random.nextInt(4) + 1;
    var offset = random.nextInt(4) + 1;

    var result = client.comment().getCommentsByUser(0, limit, offset, Order.DESC);

    assertEquals("error", result.getStatus());
    assertEquals("incorrect user ID", result.getDescription());
    assertFalse(result.isHasNext());
    assertNull(result.getComments());
  }

  @Test
  void getCommentsByProductHasNextTrue() {
    var randomComment = InitComment.initRandomComment();
    for (int i = 0; i < 10; i++) {
      service.saveComment(
        new CommentRequest(
          randomComment.getProductId(),
          randomComment.getUserId(),
          randomComment.getText()
        )).share().block();
    }
    Random random = new Random();
    var limit = random.nextInt(4) + 1;
    var offset = random.nextInt(4) + 1;

    var result = client.comment().getCommentsByProduct(randomComment.getProductId(), limit, offset, Order.DESC);

    assertEquals("ok", result.getStatus());
    assertTrue(result.isHasNext());
    assertNotNull(result.getComments());
  }

  @Test
  void getCommentsByProductHasNextFalse() {
    var randomComment = InitComment.initRandomComment();
    for (int i = 0; i < 10; i++) {
      service.saveComment(
        new CommentRequest(
          randomComment.getProductId(),
          randomComment.getUserId(),
          randomComment.getText()
        )).share().block();
    }
    var result = client.comment().getCommentsByProduct(randomComment.getProductId(), 1000, 0, Order.DESC);

    assertEquals("ok", result.getStatus());
    assertFalse(result.isHasNext());
    assertNotNull(result.getComments());
  }

  @Test
  void getCommentsWithWrongProductId() {
    Random random = new Random();
    var limit = random.nextInt(4) + 1;
    var offset = random.nextInt(4) + 1;

    var result = client.comment().getCommentsByProduct(0, limit, offset, Order.DESC);

    assertEquals("error", result.getStatus());
    assertEquals("incorrect product ID", result.getDescription());
    assertFalse(result.isHasNext());
    assertNull(result.getComments());
  }

  @Test
  void saveComment() {
    var randomComment = InitComment.initRandomComment();
    CommentRequest newComment =
      new CommentRequest(
        randomComment.getProductId(),
        randomComment.getUserId(),
        randomComment.getText()
      );
    CommentResponse result = client.comment().saveComment(newComment);

    assertEquals("ok", result.getStatus());
    assertEquals("comment saved", result.getDescription());
    assertNotNull(result.getComment().getId());
    assertEquals(newComment.getProductId(), result.getComment().getProductId());
    assertEquals(newComment.getUserId(), result.getComment().getUserId());
    assertEquals(newComment.getText(), result.getComment().getText());
    assertNotNull(result.getComment().getDate());
  }

  @Test
  void saveCommentWithNullUser() {
    var randomComment = InitComment.initRandomComment();
    var newComment =
      new CommentRequest(
        randomComment.getProductId(),
        null,
        randomComment.getText()
      );
    var result = client.comment().saveComment(newComment);

    assertEquals("error", result.getStatus());
    assertEquals("incorrect user ID", result.getDescription());
    assertNull(result.getComment());
  }

  @Test
  void saveCommentWithNullProduct() {
    var randomComment = InitComment.initRandomComment();
    var newComment =
      new CommentRequest(
        null,
        randomComment.getUserId(),
        randomComment.getText()
      );
    var result = client.comment().saveComment(newComment);

    assertEquals("error", result.getStatus());
    assertEquals("incorrect product ID", result.getDescription());
    assertNull(result.getComment());
  }

  @Test
  void saveCommentWithNullText() {
    var randomComment = InitComment.initRandomComment();
    var newComment =
      new CommentRequest(
        randomComment.getProductId(),
        randomComment.getUserId(),
        null
      );
    var result = client.comment().saveComment(newComment);

    assertEquals("error", result.getStatus());
    assertEquals("text must be not empty", result.getDescription());
    assertNull(result.getComment());
  }

  @Test
  void deleteById() {
    var randomComment = InitComment.initRandomComment();
    var comment = service.saveComment(
      new CommentRequest(
        randomComment.getProductId(),
        randomComment.getUserId(),
        randomComment.getText()
      )).share().block();
    assert comment != null;
    int id = comment.getComment().getId();
    var result = client.comment().deleteById(id, randomComment.getProductId(), randomComment.getUserId());
    assertEquals("ok", result.getStatus());
  }

  @Test
  void deleteByWrongId() {
    var result = client.comment().deleteById(99999, 4, 4);
    assertEquals("error", result.getStatus());
  }
}
