package org.marketplace.comment.service.data.service;

import org.junit.jupiter.api.Test;
import org.marketplace.comment.model.request.CommentRequest;
import org.marketplace.comment.model.response.CommentListResponse;
import org.marketplace.comment.model.response.CommentResponse;
import org.marketplace.comment.model.response.DeleteResponse;
import org.marketplace.comment.model.utils.Order;
import org.marketplace.comment.service.AbstractIntegrationTest;
import org.marketplace.utils.InitComment;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.test.StepVerifier;

import java.util.ArrayList;

class CommentServiceTest extends AbstractIntegrationTest {

  @Autowired
  private CommentService service;
  private static final String OK = "ok";
  private static final String ERROR = "error";
  private static final String INCORRECT_ID = "incorrect id";
  private static final String INCORRECT_USER_ID = "incorrect user ID";
  private static final String INCORRECT_PRODUCT_ID = "incorrect product ID";


  @Test
  void saveAndGetCommentById() {
    var randomComment = InitComment.initRandomComment();
    var result = service.saveComment(
      new CommentRequest(
        randomComment.getProductId(),
        randomComment.getUserId(),
        randomComment.getText()
      )).map(commentResponse -> commentResponse.getComment().getId()).flatMap(id -> service.getCommentById(id));

    StepVerifier.create(result.map(CommentResponse::getStatus))
      .expectNext(OK)
      .verifyComplete();
    StepVerifier.create(result.map(CommentResponse::getDescription))
      .expectNext("")
      .verifyComplete();
    StepVerifier.create(result.map(commentResponse -> commentResponse.getComment().getProductId()))
      .expectNext(randomComment.getProductId())
      .verifyComplete();
    StepVerifier.create(result.map(commentResponse -> commentResponse.getComment().getUserId()))
      .expectNext(randomComment.getUserId())
      .verifyComplete();
    StepVerifier.create(result.map(commentResponse -> commentResponse.getComment().getText()))
      .expectNext(randomComment.getText())
      .verifyComplete();

  }


  @Test
  void getCommentByZeroId() {
    StepVerifier.create(service.getCommentById(0))
      .expectNext(
        new CommentResponse(
          ERROR,
          INCORRECT_ID,
          null))
      .verifyComplete();
  }

  @Test
  void getCommentByMinusId() {
    StepVerifier.create(service.getCommentById(-23))
      .expectNext(
        new CommentResponse(
          ERROR,
          INCORRECT_ID,
          null))
      .verifyComplete();
  }

  @Test
  void getCommentsByUserHasNextTrue() {
    var randomComment = InitComment.initRandomComment();
    var result = service.saveComment(
      new CommentRequest(
        randomComment.getProductId(),
        randomComment.getUserId(),
        randomComment.getText()
      )).flatMap(commentResponse -> service.getCommentsByUser(randomComment.getUserId(), 0, 0, Order.DESC));


    StepVerifier.create(result)
      .expectNext(
        new CommentListResponse(
          OK,
          "",
          true,
          new ArrayList<>()))
      .verifyComplete();
  }

  @Test
  void getCommentsByUserHasNextFalse() {
    var randomComment = InitComment.initRandomComment();
    var result = service.saveComment(
      new CommentRequest(
        randomComment.getProductId(),
        randomComment.getUserId(),
        randomComment.getText()
      )).flatMap(commentResponse -> service.getCommentsByUser(randomComment.getUserId(), 5, 2, Order.DESC));
    StepVerifier.create(result)
      .expectNext(
        new CommentListResponse(
          OK,
          "",
          false,
          new ArrayList<>()))
      .verifyComplete();
  }

  @Test
  void getCommentsWithWrongUserId() {


    StepVerifier.create(service.getCommentsByUser(0, 1, 0, Order.DESC))
      .expectNext(
        new CommentListResponse(
          ERROR,
          INCORRECT_USER_ID,
          false,
          null))
      .verifyComplete();
  }

  @Test
  void getCommentsByProductHasNextTrue() {
    var randomComment = InitComment.initRandomComment();
    var result = service.saveComment(
      new CommentRequest(
        randomComment.getProductId(),
        randomComment.getUserId(),
        randomComment.getText()
      )).flatMap(commentResponse -> service.getCommentsByProduct(randomComment.getProductId(), 0, 0, Order.DESC));

    StepVerifier.create(result)
      .expectNext(
        new CommentListResponse(
          OK,
          "",
          true,
          new ArrayList<>()))
      .verifyComplete();
  }

  @Test
  void getCommentsByProductHasNextFalse() {
    var randomComment = InitComment.initRandomComment();
    var result = service.saveComment(
      new CommentRequest(
        randomComment.getProductId(),
        randomComment.getUserId(),
        randomComment.getText()
      )).flatMap(commentResponse -> service.getCommentsByProduct(randomComment.getProductId(), 5, 2, Order.DESC));
    StepVerifier.create(result)
      .expectNext(
        new CommentListResponse(
          OK,
          "",
          false,
          new ArrayList<>()))
      .verifyComplete();
  }

  @Test
  void getCommentsWithWrongProductId() {

    StepVerifier.create(service.getCommentsByProduct(0, 3, 0, Order.DESC))
      .expectNext(
        new CommentListResponse(
          ERROR,
          INCORRECT_PRODUCT_ID,
          false,
          null)
      ).verifyComplete();
  }

  @Test
  void saveCommentWithNullUser() {
    var randomComment = InitComment.initRandomComment();
    var newComment =
      new CommentRequest(
        randomComment.getProductId(),
        null,
        randomComment.getText()
      );

    StepVerifier.create(service.saveComment(newComment))
      .expectNext(new CommentResponse(ERROR, INCORRECT_USER_ID, null)).verifyComplete();
  }

  @Test
  void saveCommentWithNullProduct() {
    var randomComment = InitComment.initRandomComment();
    var newComment =
      new CommentRequest(
        null,
        randomComment.getUserId(),
        randomComment.getText()
      );
    StepVerifier.create(service.saveComment(newComment))
      .expectNext(new CommentResponse(ERROR, INCORRECT_PRODUCT_ID, null)).verifyComplete();
  }

  @Test
  void saveCommentWithNullText() {
    var randomComment = InitComment.initRandomComment();
    var newComment =
      new CommentRequest(
        randomComment.getProductId(),
        randomComment.getUserId(),
        null
      );
    StepVerifier.create(service.saveComment(newComment))
      .expectNext(new CommentResponse(ERROR, "text must be not empty", null)).verifyComplete();
  }

  @Test
  void deleteById() {
    var randomComment = InitComment.initRandomComment();
    var result = service.saveComment(
      new CommentRequest(
        randomComment.getProductId(),
        randomComment.getUserId(),
        randomComment.getText()
      )).map(commentResponse -> commentResponse.getComment().getId())
      .flatMap(id ->  service.deleteById(id, randomComment.getProductId(), randomComment.getUserId()));

    StepVerifier.create(result)
      .expectNext(new DeleteResponse(OK, "comment deleted")).verifyComplete();
  }

  @Test
  void deleteByWrongId() {
    StepVerifier.create(service.deleteById(99999, 4, 4))
      .expectNext(new DeleteResponse(ERROR, "comment not found")).verifyComplete();

  }
}
