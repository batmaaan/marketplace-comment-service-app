package org.marketplace.utils;

import org.marketplace.comment.model.Comment;
import org.testcontainers.shaded.org.apache.commons.lang3.RandomStringUtils;

import java.util.Random;


public class InitComment {

  public static Comment initRandomComment() {
    Random random = new Random();
    return Comment.builder()
      .id(null)
      .userId(random.nextInt(10) + 1)
      .productId(random.nextInt(10) + 1)
      .text(RandomStringUtils.randomAlphabetic(10))
      .date(null)
      .build();
  }


}
